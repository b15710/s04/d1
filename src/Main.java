public class Main {
    public static void main(String[] args) {

        //these are default constructor
        Car car1 = new Car();
//
//        car1.make = "Veyron";
//        car1.brand = "Bugati";
//        car1.price = 2000000;
//
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        //System.out.println(car1); - output will be showed a memory only / location
//
//        //Instance - object created from a class and each instance of a class should be independent from one another.
//
       Car car2 = new Car();
//
//        car2.make = "Tamaraw FX";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//        //We cannot add value to a property that does not exist or defined in our class.
//        //car2.driver = "Alejandro";
//
        Car car3 = new Car();
//
//        car3.make = "Vios";
//        car3.brand = "Toyota";
//        car3.price = 550000;
//
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);
//
        Car car4 = new Car();
//        System.out.println(car4.brand);
//
//        //parameterized constructor

        Driver driver1 = new Driver("Alejandro",25);
        Car car5 = new Car("Vios", "Toyota", 1500000, driver1);
//        System.out.println(car5.make);
//        System.out.println(car5.brand);
//        System.out.println(car5.price);

        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();

        //make property getters
        System.out.println(car1.getMake());
        System.out.println(car5.getMake());

        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car5.setMake("Innova");
        System.out.println(car5.getMake());

        car1.setBrand("Jeep");
        car1.setPrice(50000);

        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());

        //class contains an instance of another class

     //carDriver getter
        System.out.println(car5.getCarDriver().getName());

        Driver newDriver = new Driver("Antonio", 21);
        car5.setCarDriver(newDriver);

        //get name of the new carDriver
     System.out.println(car5.getCarDriver().getName());
     System.out.println(car5.getCarDriver().getAge());

     //custom getCarDriverName method
     System.out.println(car5.getCarDriverName());

     //object has object from another class

     /*
     Activity

     */

     Animal animal = new Animal("Chonky", "Black");

     animal.call();

    }
}