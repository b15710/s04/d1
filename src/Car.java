public class Car {

    //Properties/attributes - the characteristics of the object the class will create.
    //Constructor - method to create and instantiate with its initiated with its initialized values
    //Getters and setters - are methods to get the values of an objects properties or set them.
    //Methods - actions that an object can perform or do.

    //Access Modifiers - if not declared , it can only be access inside a package

    //public access - the variable/property in the class is accessible anywhere in the application.
    //Attributes/properties of a class should not be made public. They should only be accessed with getters and setters instead of just dot notation.
    //private - limits the access and ability to get or set a variable/method to only within


    //setter - methods that allow us to set the value of a property

    private String make;

    private String brand;

    private int price;

    private Driver carDriver;

    //Constructor is a method which allows us to set the initial values of an instance
    //empty/default constructor - allows us to create an instance with default initialized values.
    //By default, Java, when you class does not have a constructor, assigns one for you. Java also sets the default values. You could have a way to add your own default values.

    public Car() {

        //this.brand = "Geely";
        this.carDriver = new Driver();

    }
     public Car(String make, String brand, int price, Driver driver) {
        //parameterized constructor

        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;

     }

     //Getters and Setters for our properties
    //Getters return a value so therefore we must add the dataTyoe of the values returned.
    public String getMake() {
        //it has string so it can retrun a data
        //this keyword refers to the object/instance where the constructor or setter/getter is.
        return this.make;


    }

    public String getBrand() {

        return this.brand;
    }

    public int getPrice() {
        return this.price;
    }

    public void setMake(String makeParams) {

        this.make = makeParams;

    }

    public void setBrand(String makeParams) {

        this.brand = makeParams;

    }

    public void setPrice(int price) {
        this.price = price;
    }

    //Mini Activity


     //Methods are function of an object/instance which allows us to perform certain tasks.
    //void - means that the function does not return anything. Because in Java, a method's return dataType must also be declared.
     public void start() {

         System.out.println("Vroom! Vroom!");


     }

    //classes have relationship
    /*
    Composition allows modelling objects to be made up of other objects. Classes can have instances of other classes.

    A Car has a Driver.

    */

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    //custom method to retrieve the car driver's name:
    public String getCarDriverName(){

        return this.carDriver.getName();

    }
}
